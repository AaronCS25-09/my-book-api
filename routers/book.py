from fastapi import Depends, Path, Query
from fastapi.responses import JSONResponse
from typing import List
from config.database import Session
from models.book import Book as BookModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from services.book import BookService
from fastapi import APIRouter
from schemas.book import Book

router = APIRouter()

@router.get("/books", tags= ["Books"], response_model= List[Book], status_code= 200, dependencies= [Depends(JWTBearer())])
#@router.get("/books", tags= ["Books"], response_model= List[Book], status_code= 200)
def get_books() -> List[Book]:
    db = Session()
    result = BookService(db).get_books()
    return JSONResponse(status_code= 200, content= jsonable_encoder(result))

@router.get("/books/{id}", tags=["Books"], response_model= Book, status_code= 200)
def get_books(id: int = Path(ge= 1, le= 30)) -> Book:
    db = Session()
    result = BookService(db).get_book(id)
    if not result:
        return JSONResponse(status_code= 404, content={"message": f"Book with id: {id} does not exist"})
    return JSONResponse(status_code= 200, content= jsonable_encoder(result))

@router.get("/books/", tags=["Books"], response_model= List[Book], status_code= 200)
def get_books_by_category(category: str = Query(min_length= 5, max_length= 35), year: int = Query(le = 2023)) -> List[Book]:
    db = Session()
    result = BookService(db).get_books_by_category(category)
    if not result:
        return JSONResponse(status_code= 404, content={"message": f"Book with category: {category} & year: {year} does not exist"})
    return JSONResponse(status_code= 200, content= jsonable_encoder(result))

@router.post("/books", tags=["Books"], response_model= dict, status_code= 201)
def create_book(book: Book) -> dict:
    db = Session()
    BookService(db).create_book(book)
    return JSONResponse(status_code= 201, content= {"message": "The book was created successfully"})

@router.put("/books/{id}", tags=["Books"], response_model= dict, status_code= 200)
def update_book(id: int, book: Book) -> dict:
    db = Session()
    result = BookService(db).get_book(id)
    if not result:
        return JSONResponse(status_code= 404, content={"message": f"Book with id: {id} does not exist"})
    BookService(db).update_book(id, book)
    return JSONResponse(status_code= 200, content= {"message": "The book was updated successfully"})

@router.delete("/books/{id}", tags= ["Books"], response_model= dict)
def delete_book(id: int) -> dict:
    db = Session()
    result = BookService(db).get_book(id)
    if not result:
        return JSONResponse(status_code= 404, content={"message": f"Book with id: {id} does not exist"})
    BookService(db).delete_book(id)
    return JSONResponse(status_code= 200, content= {"message": "The book was deleted successfully"})