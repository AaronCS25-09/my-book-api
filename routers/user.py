from schemas.user import User
from fastapi import APIRouter
from utils.jwt_manager import create_token
from fastapi.responses import JSONResponse

user_router = APIRouter()

@user_router.post('/login', tags= ["auth"])
def login(user: User):
    if user.email == "aaron.camacho.25.09@gmail.com" and user.password == "123456789":
        token: str = create_token(user.dict())
    return JSONResponse(status_code= 200, content= token)