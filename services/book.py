from models.book import Book as BookModel
from schemas.book import Book

class BookService():

    def __init__(self, db) -> None:
        self.db = db

    def get_books(self):
        result = self.db.query(BookModel).all()
        return result
    
    def get_book(self, id):
        result = self.db.query(BookModel).filter(BookModel.id == id).first()
        return result
    
    def get_books_by_category(self, category):
        result = self.db.query(BookModel).filter(BookModel.category == category).all()
        return result
    
    def create_book(self, book: Book):
        new_book = BookModel(**book.dict())
        self.db.add(new_book)
        self.db.commit()
        return 
    
    def update_book(self, id: int, data: Book):
        book = self.db.query(BookModel).filter(BookModel.id == id).first()
        book.title = data.title
        book.overview = data.overview
        book.year = data.year
        book.rating = data.rating
        book.category = data.category
        self.db.commit()
        return

    def delete_book(self, id: int):
        book = self.db.query(BookModel).filter(BookModel.id == id).first()
        self.db.delete(book)
        self.db.commit()