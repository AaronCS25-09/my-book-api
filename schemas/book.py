from pydantic import BaseModel, Field
from typing import Optional

class Book(BaseModel):
    # id: int | None = None
    id: Optional[int] = None
    title: str = Field(min_length= 5, max_length=50)
    overview: str = Field(min_length= 7, max_length=850)
    year: int = Field(le=2023)
    rating: float = Field(ge= 0, le= 10)
    category: str = Field(min_length= 5, max_length=50)

    class Config:
        schema_extra = {
            "example": {
                "id": 1,
                "title": "La palabra del mudo",
                "overview": "Incididunt nisi pariatur nulla do magna duis proident aute Lorem.",
                "year": 2023,
                "rating": 8.7,
                "category": "Action"
            }
        }