from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from routers.book import router
from routers.user import user_router


app = FastAPI()
app.title = "Mi propia API"
app.version = "0.0.1"

app.add_middleware(ErrorHandler)
app.include_router(user_router)
app.include_router(router)

Base.metadata.create_all(bind= engine)

# Data

books = [
        {
        "id": 1,
        "title": "SPY x FAMILY vol.01",
        "overview": "'Spy x Family' comienza con una nueva misión para Twilight, el mejor agente de la República de Westalis. El espía debe acercarse a un objetivo bastante esquivo, y la única manera de hacerlo asumir la identidad de Loid Forger, un respetable padre de familia.",
        "year": 2019,
        "rating": 9.3,
        "category": "Action"
        },
        {
        "id": 2,
        "title": "SPY x FAMILY vol.02",
        "overview": "'Spy x Family' comienza con una nueva misión para Twilight, el mejor agente de la República de Westalis. El espía debe acercarse a un objetivo bastante esquivo, y la única manera de hacerlo asumir la identidad de Loid Forger, un respetable padre de familia.",
        "year": 2019,
        "rating": 9.4,
        "category": "Comedy"
        },
        {
        "id": 3,
        "title": "SPY x FAMILY vol.03",
        "overview": "'Spy x Family' comienza con una nueva misión para Twilight, el mejor agente de la República de Westalis. El espía debe acercarse a un objetivo bastante esquivo, y la única manera de hacerlo asumir la identidad de Loid Forger, un respetable padre de familia.",
        "year": 2020,
        "rating": 9.7,
        "category": "Family"
        }
]


# Routes

@app.get('/', tags= ["Home"])
def message():
    return HTMLResponse("<h1>Hello Micho</h1>")

