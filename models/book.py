from config.database import Base
from sqlalchemy import Column, Integer, String, Float

class Book(Base):

    __tablename__ = "books"
    id = Column(Integer, primary_key= True)
    title = Column(String)
    overview = Column(String)
    year = Column(Integer)
    rating = Column(Float)
    category = Column(String)
